#Bootstrap Tag Autocomplete

This is a bootstrap plugin to autocomplete tags for contenteditable div elements. It works in the same way tagging people on Facebook, Twitter or Sandglaz works.

#Demo and Documentation

<a href="http://sandglaz.github.com/bootstrap-tagautocomplete/">http://sandglaz.github.com/bootstrap-tagautocomplete/</a>